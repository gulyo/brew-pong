import { BreweryState } from "../../state";
import { ReplaySubject } from "rxjs";
import { store } from "../../store";
import { breweryStateSelector } from "./breweryStateSelector";
import { createSelector } from "@reduxjs/toolkit";
import { Brewery } from "../../../type";

export const breweryListSelector = createSelector(
  breweryStateSelector,
  (state: BreweryState): Brewery[] => state.BreweryList
);
export const breweryListSelector$ = new ReplaySubject<Brewery[]>();
store.subscribe(() =>
  breweryListSelector$.next(breweryListSelector(store.getState()))
);
