import { createSelector } from "@reduxjs/toolkit";
import { breweryStateSelector } from "./breweryStateSelector";
import { BreweryState } from "../../state";
import { ReplaySubject } from "rxjs";
import { store } from "../../store";

export const resultUrlSelector = createSelector(
  breweryStateSelector,
  (state: BreweryState): string => state.ResultUrl
);
export const resultUrlSelector$ = new ReplaySubject<string>();
store.subscribe(() =>
  resultUrlSelector$.next(resultUrlSelector(store.getState()))
);
