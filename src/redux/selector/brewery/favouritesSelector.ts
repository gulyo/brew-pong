import { createSelector } from "@reduxjs/toolkit";
import { breweryStateSelector } from "./breweryStateSelector";
import { BreweryState } from "../../state";
import { FavouriteContainer } from "../../../type";
import { ReplaySubject } from "rxjs";
import { store } from "../../store";

export const favouritesSelector = createSelector(
  breweryStateSelector,
  (state: BreweryState): FavouriteContainer => state.Favourites
);
export const favouritesSelector$ = new ReplaySubject<FavouriteContainer>();
store.subscribe(() =>
  favouritesSelector$.next(favouritesSelector(store.getState()))
);
