import { BreweryState, RootState } from "../../state";
import { ReplaySubject } from "rxjs";
import { store } from "../../store";

export const breweryStateSelector = (state: RootState): BreweryState =>
  state.brewery;

export const breweryStateSelector$ = new ReplaySubject<BreweryState>();
store.subscribe(() =>
  breweryStateSelector$.next(breweryStateSelector(store.getState()))
);
