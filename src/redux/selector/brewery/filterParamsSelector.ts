import { createSelector } from "@reduxjs/toolkit";
import { breweryStateSelector } from "./breweryStateSelector";
import { BreweryState } from "../../state";
import { QueryParamContainer } from "../../../type";
import { ReplaySubject } from "rxjs";
import { store } from "../../store";

export const filterParamsSelector = createSelector(
  breweryStateSelector,
  (state: BreweryState): QueryParamContainer => state.FilterParams
);
export const filterParamsSelector$ = new ReplaySubject<QueryParamContainer>();
store.subscribe(() =>
  filterParamsSelector$.next(filterParamsSelector(store.getState()))
);
