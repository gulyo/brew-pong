import { createSelector } from "@reduxjs/toolkit";
import { breweryStateSelector } from "./breweryStateSelector";
import { BreweryState } from "../../state";
import { ReplaySubject } from "rxjs";
import { store } from "../../store";

export const haveMoreSelector = createSelector(
  breweryStateSelector,
  (state: BreweryState): boolean => state.HaveMore
);
export const haveMoreSelector$ = new ReplaySubject<boolean>();
store.subscribe(() =>
  haveMoreSelector$.next(haveMoreSelector(store.getState()))
);
