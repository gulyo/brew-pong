import { createSelector } from "@reduxjs/toolkit";
import { breweryStateSelector } from "./breweryStateSelector";
import { BreweryState } from "../../state";
import { QueryParamContainer } from "../../../type";
import { ReplaySubject } from "rxjs";
import { store } from "../../store";

export const resultParamsSelector = createSelector(
  breweryStateSelector,
  (state: BreweryState): QueryParamContainer => state.ResultParams
);
export const resultParamsSelector$ = new ReplaySubject<QueryParamContainer>();
store.subscribe(() =>
  resultParamsSelector$.next(resultParamsSelector(store.getState()))
);
