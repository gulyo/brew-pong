import { RootState, UIState } from "../../state";
import { ReplaySubject } from "rxjs";
import { store } from "../../store";

export const uiStateSelector = (state: RootState): UIState => state.ui;

export const uiStateSelector$ = new ReplaySubject<UIState>();
store.subscribe(() => uiStateSelector$.next(uiStateSelector(store.getState())));
