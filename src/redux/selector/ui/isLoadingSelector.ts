import { createSelector } from "@reduxjs/toolkit";
import { UIState } from "../../state";
import { ReplaySubject } from "rxjs";
import { store } from "../../store";
import { uiStateSelector } from "./uiStateSelector";

export const isLoadingSelector = createSelector(
  uiStateSelector,
  (state: UIState): boolean => state.IsLoading
);
export const isLoadingSelector$ = new ReplaySubject<boolean>();
store.subscribe(() =>
  isLoadingSelector$.next(isLoadingSelector(store.getState()))
);
