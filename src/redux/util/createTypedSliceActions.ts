import { Slice } from "@reduxjs/toolkit";

type SliceAction<TAction extends Record<string, string>> = {
  [key in keyof TAction]: string;
};
type SliceActionLite<TAction extends Record<string, string>> = {
  [key in keyof TAction]?: string;
};

export function createTypedSliceActions<TAction extends Record<string, string>>(
  actionEnum: TAction,
  slice: Slice
): SliceAction<typeof actionEnum> {
  const tmp: SliceActionLite<TAction> = {};
  for (const key in actionEnum) {
    if (slice.actions[actionEnum[key]]) {
      tmp[key] = slice.actions[actionEnum[key]].type;
    }
  }
  return tmp as SliceAction<TAction>;
}
