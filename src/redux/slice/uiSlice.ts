import { Action, createSlice, Reducer } from "@reduxjs/toolkit";
import { uiInitialState } from "../initialState";
import { UIAction } from "../action";
import { UIState } from "../state";
import { createTypedSliceActions } from "../util";
import { uiStartLoading, uiStopLoading } from "../reducer";

const slice = createSlice({
  name: "ui",
  initialState: uiInitialState,
  reducers: {
    [UIAction.START_LOADING]: uiStartLoading,
    [UIAction.STOP_LOADING]: uiStopLoading,
  },
});

export const uiSlice: Reducer<UIState, Action<UIAction>> = slice.reducer;
export const uiAction = createTypedSliceActions<typeof UIAction>(
  UIAction,
  slice
);
