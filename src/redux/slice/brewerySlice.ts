import { Action, createSlice, Reducer } from "@reduxjs/toolkit";
import { breweryInitialState } from "../initialState";
import { BreweryAction } from "../action";
import {
  breweryAddFilter,
  breweryAppendResult,
  breweryResetFilters,
  brewerySetFavourite,
  brewerySetResult,
  breweryUnsetFavourite,
} from "../reducer";
import { BreweryState } from "../state";
import { createTypedSliceActions } from "../util";

const slice = createSlice({
  name: "brewery",
  initialState: breweryInitialState,
  reducers: {
    [BreweryAction.ADD_FILTER]: breweryAddFilter,
    [BreweryAction.RESET_FILTERS]: breweryResetFilters,
    [BreweryAction.SET_FAVOURITE]: brewerySetFavourite,
    [BreweryAction.UNSET_FAVOURITE]: breweryUnsetFavourite,
    [BreweryAction.SET_RESULT]: brewerySetResult,
    [BreweryAction.APPEND_RESULT]: breweryAppendResult,
  },
});

export const brewerySlice: Reducer<
  BreweryState,
  Action<BreweryAction>
> = slice.reducer;
export const breweryAction = createTypedSliceActions<typeof BreweryAction>(
  BreweryAction,
  slice
);
