import { BreweryState } from "./BreweryState";
import { UIState } from "./UIState";

export interface RootState {
  brewery: BreweryState;
  ui: UIState;
}
