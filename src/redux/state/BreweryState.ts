import { Brewery, FavouriteContainer, QueryParamContainer } from "../../type";

export interface BreweryState {
  BreweryList: Brewery[];
  Favourites: FavouriteContainer;
  FilterParams: QueryParamContainer;
  ResultParams: QueryParamContainer;
  ResultUrl: string;
  HaveMore: boolean;
}
