import { BreweryState } from "../state";

export const breweryInitialState: BreweryState = {
  BreweryList: [],
  Favourites: {},
  FilterParams: {},
  ResultParams: {},
  ResultUrl: "",
  HaveMore: false,
};
