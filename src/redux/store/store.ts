import { configureStore } from "@reduxjs/toolkit";
import { brewerySlice, uiSlice } from "../slice";

export const store = configureStore({
  reducer: {
    brewery: brewerySlice,
    ui: uiSlice,
  },
});
