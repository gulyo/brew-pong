export enum BreweryAction {
  ADD_FILTER = "brewery/addFilter",
  RESET_FILTERS = "brewery/resetFilters",
  SET_FAVOURITE = "brewery/setFavourite",
  UNSET_FAVOURITE = "brewery/unsetFavourite",
  SET_RESULT = "brewery/setResult",
  APPEND_RESULT = "brewery/appendResult",
}
