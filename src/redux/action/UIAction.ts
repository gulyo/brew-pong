export enum UIAction {
  START_LOADING = "UI/startLoading",
  STOP_LOADING = "UI/stopLoading",
}
