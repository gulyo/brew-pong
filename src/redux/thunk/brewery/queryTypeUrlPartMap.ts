import { QueryType } from "../../../type";

type QueryTypeUrlPartMap = {
  [key in QueryType]: string;
};

// I don't want to introduce an implicit dependency between my enum and the data source
// parameters
export const queryTypeUrlPartMap: QueryTypeUrlPartMap = {
  by_city: "by_city",
  by_dist: "by_dist",
  by_name: "by_name",
  by_postal: "by_postal",
  by_state: "by_state",
  by_type: "by_type",
  search: "query",
};
