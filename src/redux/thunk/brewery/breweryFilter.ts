import { ThunkAction } from "redux-thunk";
import { RootState } from "../../state";
import { Action } from "redux";
import { breweryAction, uiAction } from "../../slice";
import { constants } from "../../../tool";
import { filterParamsSelector } from "../../selector";
import { queryTypeUrlPartMap } from "./queryTypeUrlPartMap";

export function breweryFilter(): ThunkAction<void, RootState, unknown, Action> {
  // TODO: Add error handling
  return (dispatch, getState): void => {
    dispatch({
      type: uiAction.START_LOADING,
    });
    const queryParams = filterParamsSelector(getState());
    const paramList = Object.values(queryParams);

    const urlVariables = paramList.reduce(
      (res, cur) =>
        `${res}&${queryTypeUrlPartMap[cur.Type]}=${encodeURIComponent(
          cur.Value
        )}`,
      ""
    );
    const queryUrl = `https://api.openbrewerydb.org/breweries?per_page=${constants.breweryPerPage}${urlVariables}`;
    fetch(queryUrl).then((result) => {
      result.json().then((breweryList) => {
        dispatch({
          type: breweryAction.SET_RESULT,
          payload: {
            breweryList,
            queryParams,
            queryUrl,
          },
        });
        dispatch({
          type: breweryAction.RESET_FILTERS,
        });
        dispatch({
          type: uiAction.STOP_LOADING,
        });
      });
    });
  };
}
