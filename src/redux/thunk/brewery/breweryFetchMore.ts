import { ThunkAction } from "redux-thunk";
import { RootState } from "../../state";
import { Action } from "redux";
import { breweryAction, uiAction } from "../../slice";
import { breweryListSelector, resultUrlSelector } from "../../selector";
import { constants } from "../../../tool";

export function breweryFetchMore(): ThunkAction<
  void,
  RootState,
  unknown,
  Action
> {
  // TODO: Add error handling
  return (dispatch, getState): void => {
    dispatch({
      type: uiAction.START_LOADING,
    });
    const url = resultUrlSelector(getState());
    const resultCount = breweryListSelector(getState()).length;
    const page = Math.floor(resultCount / constants.breweryPerPage) + 1;
    fetch(`${url}&page=${page}`).then((result) => {
      result.json().then((breweryList) => {
        dispatch({
          type: breweryAction.APPEND_RESULT,
          payload: {
            breweryList,
          },
        });
        dispatch({
          type: uiAction.STOP_LOADING,
        });
      });
    });
  };
}
