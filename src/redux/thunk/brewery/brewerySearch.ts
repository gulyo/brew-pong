import { QueryParam, QueryParamContainer } from "../../../type";
import { ThunkAction } from "redux-thunk";
import { RootState } from "../../state";
import { Action } from "redux";
import { breweryAction, uiAction } from "../../slice";
import { constants } from "../../../tool";
import { queryTypeUrlPartMap } from "./queryTypeUrlPartMap";

export function brewerySearch(
  param: QueryParam
): ThunkAction<void, RootState, unknown, Action> {
  // TODO: Add error handling
  return (dispatch): void => {
    dispatch({
      type: uiAction.START_LOADING,
    });
    const queryUrl = `https://api.openbrewerydb.org/breweries/search?per_page=${
      constants.breweryPerPage
    }&${queryTypeUrlPartMap[param.Type]}=${encodeURIComponent(param.Value)}`;
    fetch(queryUrl).then((result) => {
      result.json().then((breweryList) => {
        const queryParams: QueryParamContainer = {
          [param.Type]: param,
        };
        dispatch({
          type: breweryAction.SET_RESULT,
          payload: {
            breweryList,
            queryParams,
            queryUrl,
          },
        });
        dispatch({
          type: uiAction.STOP_LOADING,
        });
      });
    });
  };
}
