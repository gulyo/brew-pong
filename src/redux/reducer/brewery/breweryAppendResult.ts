import { BreweryState } from "../../state";
import { PayloadAction } from "@reduxjs/toolkit";
import { Brewery } from "../../../type";
import { constants } from "../../../tool";

interface BreweryAppendResultArg {
  breweryList: Brewery[];
}

export const breweryAppendResult = (
  state: BreweryState,
  action: PayloadAction<BreweryAppendResultArg>
): BreweryState => ({
  ...state,
  BreweryList: [...state.BreweryList, ...action.payload.breweryList],
  HaveMore: action.payload.breweryList.length == constants.breweryPerPage,
});
