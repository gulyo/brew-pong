import { BreweryState } from "../../state";
import { PayloadAction } from "@reduxjs/toolkit";
import { Brewery, FavouriteContainer } from "../../../type";

export const breweryUnsetFavourite = (
  state: BreweryState,
  action: PayloadAction<Brewery>
): BreweryState => ({
  ...state,
  Favourites: Object.keys(state.Favourites)
    .filter((id) => id !== action.payload.id)
    .reduce((res, cur) => {
      res[cur] = state.Favourites[cur];
      return res;
    }, {} as FavouriteContainer),
});
