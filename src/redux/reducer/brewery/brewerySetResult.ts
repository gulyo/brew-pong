import { BreweryState } from "../../state";
import { PayloadAction } from "@reduxjs/toolkit";
import { Brewery, QueryParamContainer } from "../../../type";
import { constants } from "../../../tool";

interface BrewerySetResultArg {
  breweryList: Brewery[];
  queryParams: QueryParamContainer;
  queryUrl: string;
}

export const brewerySetResult = (
  state: BreweryState,
  action: PayloadAction<BrewerySetResultArg>
): BreweryState => ({
  ...state,
  BreweryList: [...action.payload.breweryList],
  ResultParams: { ...action.payload.queryParams },
  ResultUrl: action.payload.queryUrl,
  HaveMore: action.payload.breweryList.length == constants.breweryPerPage,
});
