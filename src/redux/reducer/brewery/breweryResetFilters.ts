import { BreweryState } from "../../state";

export const breweryResetFilters = (state: BreweryState): BreweryState => ({
  ...state,
  FilterParams: {},
});
