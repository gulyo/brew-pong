import { BreweryState } from "../../state";
import { PayloadAction } from "@reduxjs/toolkit";
import { Brewery } from "../../../type";

export const brewerySetFavourite = (
  state: BreweryState,
  action: PayloadAction<Brewery>
): BreweryState => ({
  ...state,
  Favourites: {
    ...state.Favourites,
    [action.payload.id]: action.payload,
  },
});
