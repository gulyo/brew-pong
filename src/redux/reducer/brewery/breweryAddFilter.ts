import { BreweryState } from "../../state";
import { PayloadAction } from "@reduxjs/toolkit";
import { QueryParam } from "../../../type";

export const breweryAddFilter = (
  state: BreweryState,
  action: PayloadAction<QueryParam>
): BreweryState => ({
  ...state,
  FilterParams: {
    ...state.FilterParams,
    [action.payload.Type]: action.payload,
  },
});
