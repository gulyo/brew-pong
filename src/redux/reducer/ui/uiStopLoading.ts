import { UIState } from "../../state";

export const uiStopLoading = (state: UIState): UIState => ({
  ...state,
  IsLoading: false,
});
