import { UIState } from "../../state";

export const uiStartLoading = (state: UIState): UIState => ({
  ...state,
  IsLoading: true,
});
