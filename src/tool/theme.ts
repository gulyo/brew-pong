import { createTheme, Theme } from "@mui/material";

// TODO: Theme-ing is not that simple as it seamed at first - check what can be moved here from style sheets
export const theme: Theme = createTheme({
  palette: {
    mode: "dark",
  },
});
