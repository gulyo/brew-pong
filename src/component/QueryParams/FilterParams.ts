import { QueryParams } from "./QueryParams";
import { Observable } from "rxjs";
import { QueryParamContainer } from "../../type";
import { filterParamsSelector$ } from "../../redux/selector";

export class FilterParams extends QueryParams {
  protected get paramSelector$(): Observable<QueryParamContainer> {
    return filterParamsSelector$;
  }
}
