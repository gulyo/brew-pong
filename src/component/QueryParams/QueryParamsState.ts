import { QueryParam } from "../../type";

export interface QueryParamsState {
  QueryParamList: QueryParam[];
}
