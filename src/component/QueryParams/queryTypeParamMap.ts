import { QueryType } from "../../type";

type QueryTypeParamMap = {
  [key in QueryType]: string;
};

export const queryTypeParamMap: QueryTypeParamMap = {
  search: "Search",
  by_city: "City",
  by_type: "Brewery Type",
  by_dist: "Distance",
  by_name: "Name",
  by_postal: "Postal Code",
  by_state: "State",
};
