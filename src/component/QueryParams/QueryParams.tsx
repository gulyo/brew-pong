import { SubscriberComponent } from "../util";
import { QueryParamContainer } from "../../type";
import { map, Observable, takeUntil } from "rxjs";
import { QueryParamsState } from "./QueryParamsState";
import React from "react";
import { Badge, Chip } from "@mui/material";
import { queryTypeParamMap } from "./queryTypeParamMap";

export abstract class QueryParams extends SubscriberComponent<
  Record<string, never>,
  QueryParamsState
> {
  protected abstract get paramSelector$(): Observable<QueryParamContainer>;

  componentDidMount() {
    this.paramSelector$
      .pipe(
        map((container) => Object.values(container)),
        takeUntil(this.unsubscribe$)
      )
      .subscribe((container) =>
        this.setState({
          ...this.state,
          QueryParamList: container || [],
        })
      );
  }

  public render() {
    if (!this.state?.QueryParamList) {
      return null;
    }
    return (
      <React.Fragment>
        {this.state.QueryParamList.map((param) => (
          <Badge
            badgeContent={queryTypeParamMap[param.Type]}
            key={param.Type}
            sx={{ marginRight: (theme) => theme.spacing(3) }}
          >
            <Chip label={param.Value} />
          </Badge>
        ))}
      </React.Fragment>
    );
  }
}
