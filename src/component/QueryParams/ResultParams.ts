import { QueryParams } from "./QueryParams";
import { Observable } from "rxjs";
import { QueryParamContainer } from "../../type";
import { resultParamsSelector$ } from "../../redux/selector";

export class ResultParams extends QueryParams {
  protected get paramSelector$(): Observable<QueryParamContainer> {
    return resultParamsSelector$;
  }
}