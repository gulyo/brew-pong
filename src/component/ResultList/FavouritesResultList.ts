import { ResultList } from "./ResultList";
import { map, Observable } from "rxjs";
import { Brewery, FavouriteContainer } from "../../type";
import { favouritesSelector$ } from "../../redux/selector";

export class FavouritesResultList extends ResultList {
  protected get displaySelector$(): Observable<Brewery[]> {
    return favouritesSelector$.pipe(
      map((favourites: FavouriteContainer) => Object.values(favourites))
    );
  }
}