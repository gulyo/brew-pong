import React from "react";
import { ResultListState } from "./ResultListState";
import { Observable, takeUntil } from "rxjs";
import { Brewery } from "../../type";
import { Grid, Typography } from "@mui/material";
import { BreweryCard } from "../BreweryCard";
import { SubscriberComponent } from "../util";
import SentimentVeryDissatisfiedIcon from "@mui/icons-material/SentimentVeryDissatisfied";
import styles from "./ResultList.module.scss";

export abstract class ResultList extends SubscriberComponent<
  Record<string, never>,
  ResultListState
> {
  protected abstract get displaySelector$(): Observable<Brewery[]>;

  public componentDidMount() {
    this.displaySelector$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((displayList) => {
        this.setState({
          ...this.state,
          DisplayList: [...displayList],
        });
      });
  }

  public render() {
    if (!this.state?.DisplayList) {
      return (
        <Grid item xs={12}>
          <Typography component={"h1"} align={"center"}>
            Nothing to see here
            <SentimentVeryDissatisfiedIcon />
          </Typography>
        </Grid>
      );
    }
    return (
      <React.Fragment>
        {this.state.DisplayList?.map((brewery) => (
          <Grid
            item
            xs={12}
            md={6}
            lg={4}
            xl={3}
            key={brewery.id}
            className={styles.cardGrid}
          >
            <BreweryCard brewery={brewery} />
          </Grid>
        ))}
      </React.Fragment>
    );
  }
}
