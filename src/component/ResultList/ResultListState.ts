import { Brewery } from "../../type";

export interface ResultListState {
  DisplayList: Brewery[];
}
