import { ResultList } from "./ResultList";
import { Observable } from "rxjs";
import { Brewery } from "../../type";
import { breweryListSelector$ } from "../../redux/selector";

export class SearchResultList extends ResultList {
  protected get displaySelector$(): Observable<Brewery[]> {
    return breweryListSelector$;
  }
}