import { SubscriberComponent } from "../util";
import { isLoadingSelector$ } from "../../redux/selector";
import { takeUntil } from "rxjs";
import { LoadingState } from "./LoadingState";
import { Backdrop, CircularProgress } from "@mui/material";
import React from "react";

export class Loading extends SubscriberComponent<
  Record<string, never>,
  LoadingState
> {
  componentDidMount() {
    isLoadingSelector$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((isLoading) =>
        this.setState({
          ...this.state,
          IsLoading: isLoading,
        })
      );
  }

  public render() {
    return (
      <Backdrop
        sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={!!this.state?.IsLoading}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
    );
  }
}
