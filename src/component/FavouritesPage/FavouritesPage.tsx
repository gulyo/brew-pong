import React from "react";
import { Grid } from "@mui/material";
import { FavouritesResultList } from "../ResultList";

export class FavouritesPage extends React.Component {
  public render() {
    return (
      <Grid container spacing={2}>
        <FavouritesResultList />
      </Grid>
    );
  }
}
