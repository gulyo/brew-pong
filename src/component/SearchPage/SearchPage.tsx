import React from "react";
import { Divider, Grid } from "@mui/material";
import { FilterForm, SearchForm } from "../form";
import { SearchResultList } from "../ResultList";
import { ResultParams } from "../QueryParams";
import { FetchMore } from "./FetchMore";

export class SearchPage extends React.Component {
  public render() {
    return (
      <Grid container spacing={2}>
        <SearchForm />
        <Grid item xs={12}>
          <Divider variant={"middle"} />
        </Grid>
        <FilterForm />
        <Grid item xs={12}>
          <Divider variant={"middle"}>Result</Divider>
        </Grid>
        <Grid item xs={12}>
          <ResultParams />
        </Grid>
        <SearchResultList />
        <FetchMore />
      </Grid>
    );
  }
}
