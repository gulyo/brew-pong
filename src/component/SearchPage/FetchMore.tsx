import { SubscriberComponent } from "../util";
import { haveMoreSelector$ } from "../../redux/selector";
import { takeUntil } from "rxjs";
import { Button, Grid } from "@mui/material";
import React from "react";
import { breweryFetchMore } from "../../redux/thunk";
import { FetchMoreState } from "./FetchMoreState";
import DownloadIcon from "@mui/icons-material/Download";
import { store } from "../../redux/store";

export class FetchMore extends SubscriberComponent<
  Record<string, never>,
  FetchMoreState
> {
  public get HaveMore(): boolean {
    return !!this.state?.HaveMore;
  }

  componentDidMount() {
    haveMoreSelector$.pipe(takeUntil(this.unsubscribe$)).subscribe((haveMore) =>
      this.setState({
        ...this.state,
        HaveMore: haveMore,
      })
    );
  }

  public render() {
    if (!this.HaveMore) {
      return null;
    }
    return (
      <Grid item xs={12}>
        <Button fullWidth onClick={this.fetchMore} variant={"outlined"}>
          <DownloadIcon />
          Fetch More
        </Button>
      </Grid>
    );
  }

  private fetchMore = (): void => {
    store.dispatch(breweryFetchMore());
  };
}
