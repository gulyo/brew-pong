import React from "react";
import {
  Drawer,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Toolbar,
} from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import StarIcon from "@mui/icons-material/Star";
import HomeIcon from "@mui/icons-material/Home";
import SportsBarIcon from "@mui/icons-material/SportsBar";

import { Link } from "react-router-dom";
import { RoutePart } from "../../type";

import styles from "./Navigation.module.scss";

export class Navigation extends React.Component {
  public render() {
    return (
      <Drawer variant={"permanent"} anchor={"left"} className={styles.menu}>
        <Toolbar className={styles.toolbar}>
          <div />
          <SportsBarIcon />
        </Toolbar>
        <List className={`${styles.menu} ${styles.list}`}>
          <ListItem button component={Link} to={RoutePart.HOME}>
            <ListItemIcon>
              <HomeIcon />
            </ListItemIcon>
            <ListItemText primary={"Home"} />
          </ListItem>
          <ListItem button component={Link} to={RoutePart.SEARCH}>
            <ListItemIcon>
              <SearchIcon />
            </ListItemIcon>
            <ListItemText primary={"Search"} />
          </ListItem>
          <ListItem button component={Link} to={RoutePart.FAVOURITES}>
            <ListItemIcon>
              <StarIcon />
            </ListItemIcon>
            <ListItemText primary={"Favourites"} />
          </ListItem>
        </List>
      </Drawer>
    );
  }
}
