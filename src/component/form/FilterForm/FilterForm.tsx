import React, { SyntheticEvent } from "react";
import {
  Autocomplete,
  Button,
  Grid,
  IconButton,
  InputAdornment,
  ListItem,
  ListItemText,
  TextField,
} from "@mui/material";
import { filterTypeOptions } from "./filterTypeOptions";
import { FilterTypeOption } from "./FilterTypeOption";
import formStyles from "../Form.module.scss";
import FilterIcon from "@mui/icons-material/FilterAlt";
import AddCircleIcon from "@mui/icons-material/AddCircle";
import { FilterParams } from "../../QueryParams";
import { store } from "../../../redux/store";
import { breweryAction } from "../../../redux/slice";
import { BreweryType, breweryType, QueryParam } from "../../../type";
import { breweryFilter } from "../../../redux/thunk";

export class FilterForm extends React.Component {
  private pattern?: string;
  private filterType?: FilterTypeOption;

  public render() {
    return (
      <React.Fragment>
        <Grid item xs={12}>
          <FilterParams />
        </Grid>

        <Grid item sm={12} md={6} lg={5}>
          <Autocomplete
            id={"search-form-filter-type-input"}
            value={this.filterType}
            size={"small"}
            fullWidth
            options={filterTypeOptions}
            getOptionLabel={(option) => option.label}
            onChange={this.setType}
            renderOption={(props, option) => (
              <ListItem {...props} key={option.type}>
                <ListItemText>{option.label}</ListItemText>
              </ListItem>
            )}
            renderInput={(params) => (
              <TextField
                {...params}
                label="Select filter type"
                inputProps={{
                  ...params.inputProps,
                }}
              />
            )}
          />
        </Grid>
        <Grid item sm={12} md={6} lg={5}>
          <TextField
            id={"search-form-search-input"}
            label={"Pattern"}
            value={this.pattern}
            variant={"outlined"}
            size={"small"}
            fullWidth
            onChange={this.setPattern}
            InputProps={{
              endAdornment: (
                <InputAdornment position={"end"}>
                  <IconButton edge={"end"} onClick={this.addFilter}>
                    <AddCircleIcon />
                  </IconButton>
                </InputAdornment>
              ),
            }}
          />
        </Grid>
        <Grid item xs={12} lg={2} className={formStyles.buttonGrid}>
          <Button
            onClick={this.applyFilters}
            fullWidth
            variant={"outlined"}
            startIcon={<FilterIcon />}
          >
            Filter
          </Button>
        </Grid>
      </React.Fragment>
    );
  }

  private setType = (
    event: SyntheticEvent<Element, Event>,
    value: FilterTypeOption | null
  ): void => {
    this.filterType = value ? value : undefined;
  };

  private setPattern = (event: React.ChangeEvent<HTMLInputElement>): void => {
    this.pattern = event.target.value;
  };

  private addFilter: () => void = () => {
    if (!(this.filterType && this.pattern)) {
      console.warn("Filter type and pattern must be set to add filter");
      return;
    }
    //BreweryType check
    if (
      this.filterType?.type === "by_type" &&
      !breweryType.includes(this.pattern as BreweryType)
    ) {
      console.warn(
        "Invalid Brewery Type - check https://www.openbrewerydb.org/documentation/01-listbreweries#by_type for available types"
      );
      return;
    }

    store.dispatch({
      type: breweryAction.ADD_FILTER,
      payload: {
        Type: this.filterType.type,
        Value: this.pattern,
      } as QueryParam,
    });
  };

  private applyFilters: () => void = () => {
    this.addFilter();
    store.dispatch(breweryFilter());
  };
}
