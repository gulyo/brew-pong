import { BreweryFilterType } from "../../../type";

export interface FilterTypeOption {
  type: BreweryFilterType;
  label: string;
}
