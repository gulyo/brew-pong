import { FilterTypeOption } from "./FilterTypeOption";
import { BreweryFilterType } from "../../../type";

type FilterTypeOptionMap = {
  [key in BreweryFilterType]: {
    weight: number;
    label: string;
  };
};

const filterTypeOptionMap: FilterTypeOptionMap = {
  by_name: { weight: 100, label: "Name" },
  by_city: { weight: 200, label: "City" },
  by_dist: { weight: 300, label: "Coordinates" },
  by_type: { weight: 400, label: "Type" },
  by_postal: { weight: 500, label: "Postal Code" },
  by_state: { weight: 600, label: "State" },
};

export const filterTypeOptions: FilterTypeOption[] = Object.entries(
  filterTypeOptionMap
)
  .sort((a, b) => a[1].weight - b[1].weight)
  .map((t) => ({ type: t[0] as BreweryFilterType, label: t[1].label }));
