import React from "react";
import { Button, Grid, TextField } from "@mui/material";
import formStyles from "../Form.module.scss";
import SearchIcon from "@mui/icons-material/Search";
import { brewerySearch } from "../../../redux/thunk";
import { store } from "../../../redux/store";

export class SearchForm extends React.Component {
  private query?: string;

  public render() {
    return (
      <React.Fragment>
        <Grid item xs={12} md={10}>
          <TextField
            id={"search-form-search-input"}
            label={"Query"}
            value={this.query}
            variant={"outlined"}
            size={"small"}
            fullWidth
            onChange={this.setQuery}
          />
        </Grid>
        <Grid item xs={12} md={2} className={formStyles.buttonGrid}>
          <Button
            startIcon={<SearchIcon />}
            fullWidth
            variant={"outlined"}
            onClick={this.search}
          >
            Search
          </Button>
        </Grid>
      </React.Fragment>
    );
  }

  private search = (): void => {
    if (!this.query) {
      console.error("Search query cannot be empty");
      return;
    }
    store.dispatch(
      brewerySearch({
        Type: "search",
        Value: this.query,
      })
    );
  };

  private setQuery = (event: React.ChangeEvent<HTMLInputElement>): void => {
    this.query = event.target.value;
  };
}
