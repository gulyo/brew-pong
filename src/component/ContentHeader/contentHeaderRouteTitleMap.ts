import { RoutePart } from "../../type";

type ContentHeaderRouteTitleMap = {
  [key in RoutePart]: string;
};

export const contentHeaderRouteTitleMap: ContentHeaderRouteTitleMap = {
  [RoutePart.HOME]: "Home",
  [RoutePart.SEARCH]: "Search",
  [RoutePart.FAVOURITES]: "Favourites",
};
