import React from "react";
import { ContentHeaderProps } from "./ContentHeaderProps";
import { contentHeaderRouteTitleMap } from "./contentHeaderRouteTitleMap";
import { AppBar, Toolbar, Typography } from "@mui/material";
import styles from "./ContentHeader.module.scss";

export class ContentHeader extends React.Component<ContentHeaderProps> {
  public get Title(): string {
    return contentHeaderRouteTitleMap[this.props.routePart];
  }

  public render() {
    return (
      <AppBar position={"sticky"} elevation={0}>
        <Toolbar className={styles.headerToolbar} variant={"regular"}>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            {this.Title}
          </Typography>
        </Toolbar>
      </AppBar>
    );
  }
}
