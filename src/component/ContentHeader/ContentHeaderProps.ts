import { RoutePart } from "../../type";

export interface ContentHeaderProps {
  routePart: RoutePart;
}
