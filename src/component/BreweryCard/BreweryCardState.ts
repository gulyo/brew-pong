import { FavouriteContainer } from "../../type";

export interface BreweryCardState {
  Favourites: FavouriteContainer;
}
