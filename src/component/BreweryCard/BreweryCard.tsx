import { BreweryCardProps } from "./BreweryCardProps";
import React from "react";
import { Brewery } from "../../type";
import {
  Card,
  CardActions,
  CardContent,
  Grid,
  IconButton,
  Typography,
} from "@mui/material";
import { SubscriberComponent } from "../util";
import { takeUntil } from "rxjs";
import { favouritesSelector$ } from "../../redux/selector";
import { BreweryCardState } from "./BreweryCardState";
import LinkIcon from "@mui/icons-material/Link";
import MyLocationIcon from "@mui/icons-material/MyLocation";
import FavoriteIcon from "@mui/icons-material/Favorite";
import FavoriteBorderIcon from "@mui/icons-material/FavoriteBorder";

import styles from "./BreweryCard.module.scss";
import { store } from "../../redux/store";
import { breweryAction } from "../../redux/slice";

export class BreweryCard extends SubscriberComponent<
  BreweryCardProps,
  BreweryCardState
> {
  public get IsFavourite(): boolean {
    if (!this.props?.brewery?.id) {
      return false;
    }
    return this.state?.Favourites
      ? !!this.state.Favourites[this.props.brewery.id]
      : false;
  }

  public componentDidMount() {
    favouritesSelector$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((favourites) => {
        this.setState({
          ...this.state,
          Favourites: { ...favourites },
        });
      });
  }

  public render() {
    const brewery: Brewery = this.props.brewery;
    return (
      <React.Fragment>
        <Card className={styles.fullCard}>
          <CardContent>
            <Typography variant={"h5"}>{brewery.name}</Typography>
            <Typography>{brewery.country}</Typography>
            <Typography>{brewery.city}</Typography>
          </CardContent>
          <CardActions>
            <Grid item className={styles.gridCenter} xs={1}>
              <a
                href={brewery.website_url}
                target="_blank"
                rel="noreferrer noopener"
              >
                <LinkIcon />
              </a>
            </Grid>
            <Grid item className={styles.gridCenter} xs={1}>
              <a
                href={`https://www.google.com/maps/place/${brewery.latitude},${brewery.longitude}`}
                target="_blank"
                rel="noreferrer noopener"
              >
                <MyLocationIcon />
              </a>
            </Grid>
            <Grid item className={styles.gridRight} xs={10}>
              <IconButton
                className={styles.bigIcon}
                onClick={this.toggleFavourite}
              >
                {this.IsFavourite ? <FavoriteIcon /> : <FavoriteBorderIcon />}
              </IconButton>
            </Grid>
          </CardActions>
        </Card>
      </React.Fragment>
    );
  }

  private toggleFavourite = (): void => {
    store.dispatch({
      type: this.IsFavourite
        ? breweryAction.UNSET_FAVOURITE
        : breweryAction.SET_FAVOURITE,
      payload: this.props.brewery,
    });
  };
}
