import { Brewery } from "../../type";

export interface BreweryCardProps {
  brewery: Brewery;
}
