import React from "react";
import README from "../../README.md";
import Markdown from "marked-react";
import { Container } from "@mui/material";

export class HomePage extends React.Component {
  private md = "";

  public componentDidMount() {
    (async () => {
      const response = await fetch(README);
      this.md = await response.text();
      this.setState({});
    })();
  }

  public render() {
    return (
      <Container>
        <Markdown>{this.md}</Markdown>
      </Container>
    );
  }
}
