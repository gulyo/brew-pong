import React from "react";
import { Subject } from "rxjs";

export abstract class SubscriberComponent<
  TProps = Record<string, unknown>,
  TState = Record<string, unknown>
> extends React.Component<TProps, TState> {
  protected unsubscribe$: Subject<boolean> = new Subject<boolean>();

  public componentWillUnmount() {
    this.unsubscribe$.next(true);
    this.unsubscribe$.complete();
  }
}