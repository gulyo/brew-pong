import React from "react";
import { Route, Routes } from "react-router-dom";
import { RoutePart } from "./type";
import {
  ContentHeader,
  FavouritesPage,
  HomePage,
  Loading,
  Navigation,
  SearchPage,
} from "./component";
import { Container, ThemeProvider } from "@mui/material";
import { theme } from "./tool";
import styles from "./App.module.scss";

export class App extends React.Component {
  public render() {
    return (
      <ThemeProvider theme={theme}>
        <Navigation />
        <Routes>
          <Route
            path={RoutePart.HOME}
            element={<ContentHeader routePart={RoutePart.HOME} />}
          />
          <Route
            path={RoutePart.SEARCH}
            element={<ContentHeader routePart={RoutePart.SEARCH} />}
          />
          <Route
            path={RoutePart.FAVOURITES}
            element={<ContentHeader routePart={RoutePart.FAVOURITES} />}
          />
        </Routes>
        <div className={styles.content}>
          <Container>
            <Routes>
              <Route path={RoutePart.HOME} element={<HomePage />} />
              <Route path={RoutePart.SEARCH} element={<SearchPage />} />
              <Route path={RoutePart.FAVOURITES} element={<FavouritesPage />} />
            </Routes>
          </Container>
        </div>
        <Loading />
      </ThemeProvider>
    );
  }
}

export default App;
