import { QueryType } from "./QueryType";
import { QueryParam } from "./QueryParam";

export type QueryParamContainer = {
  [key in QueryType]?: QueryParam;
};
