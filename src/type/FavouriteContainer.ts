import { Brewery } from "./OpenBreweryDb";

export type FavouriteContainer = Record<string, Brewery>;
