import { BreweryFilterType } from "./OpenBreweryDb";

export type QueryType = BreweryFilterType | "search";
