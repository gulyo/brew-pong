export enum RoutePart {
  HOME = "/",
  SEARCH = "search",
  FAVOURITES = "favourites",
}
