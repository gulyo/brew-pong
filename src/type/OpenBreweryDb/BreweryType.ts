export const breweryType = [
  "micro",
  "nano",
  "regional",
  "brewpub",
  "large",
  "planning",
  "bar",
  "contract",
  "proprietor",
  "closed",
] as const;

export type BreweryType = typeof breweryType[number];
