import { BreweryType } from "./BreweryType";
import { BreweryAutocompleteItem } from "./BreweryAutocompleteItem";

export interface Brewery extends BreweryAutocompleteItem {
  brewery_type: BreweryType;
  street?: string;
  address_2?: string;
  address_3?: string;
  city?: string;
  state?: string;
  county_province?: string;
  postal_code?: string;
  country?: string;
  longitude?: string;
  latitude?: string;
  phone?: string;
  website_url?: string;
  updated_at?: string;
  created_at?: string;
}
