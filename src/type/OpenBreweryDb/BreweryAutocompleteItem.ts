export interface BreweryAutocompleteItem {
  id: string;
  name: string;
}
