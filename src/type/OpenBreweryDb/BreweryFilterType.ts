export const breweryFilterType = [
  "by_name",
  "by_city",
  "by_dist",
  "by_state",
  "by_postal",
  "by_type",
] as const;
export type BreweryFilterType = typeof breweryFilterType[number];
