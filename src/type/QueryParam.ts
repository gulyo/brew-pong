import { QueryType } from "./QueryType";

export interface QueryParam {
  Type: QueryType;
  Value: string;
}
