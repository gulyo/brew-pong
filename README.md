# Brew Pong

Brewery finding sample project using Open Brewery Db as a back-end

## 1. Install & run

 - clone the git repository
 - run `nvm install` to install the appropriate nodejs version
 - run `npm install` to install project dependencies
 - run `npm run start`, Brew Pong will be accessible at [http://localhost:3000](http://localhost:3000)

Alternatively you can google up how to build and serve a React project :)

## 2. Goal

Check / try / update knowledge on the following FE stack:

 - React + redux + rxjs
 - Typescript
 - Scss modules
 - Material UI

using the latest versions (check `package.json`)

## 3. Specification

Create a sample project

 - using [Open Brewery Db](https://www.openbrewerydb.org/) as data source / back-end
 - provide search feature & display results
 - provide a favourites feature (valid for the session)

## 4. Usage

This app was implemented with desktop displays on mind.
It has some level of responsiveness, but it's not appropriate for mobile mode usage.

### 4.1 Home

*Home* page display the HTML representation of theis `README.md` file.

### 4.2 Search

*Search* page has three parts.

#### 4.2.1 Search form

After providing a query (e.g.: *dog*), you can run a brewery search.
Clicking the *SEARCH* button will run a query against the
[search feature](https://www.openbrewerydb.org/documentation/03-search)
of the back-end.

#### 4.2.1 Filter form

This part has two main features
 - You can select a filter type, provide a pattern and add it to the list of filters. Use the *+* button icon in the *Pattern* text field to do so.
 - You can apply the filters by clicking the *FILTER* button.

This will use the [List Breweries](https://www.openbrewerydb.org/documentation/01-listbreweries)
feature of the back-end.

Given there was a type selected and a pattern provided but not added to the list, the *FILTER* button will
try to add them to the list before executing the query.

##### 4.2.1.1 Validation

If you provide an invalid pattern, or no type, the page will send you a warning in the console.

##### 4.2.1.2 Brewery types

The *Type* filter accepts the following [values](https://www.openbrewerydb.org/documentation/01-listbreweries#by_type)
 - *micro* - Most craft breweries. For example, Samual Adams is still considered a micro brewery.
 - *nano* - An extremely small brewery which typically only distributes locally.
 - *regional* - A regional location of an expanded brewery. Ex. Sierra Nevada's Asheville, NC location.
 - *brewpub* - A beer-focused restaurant or restaurant/bar with a brewery on-premise.
 - *large* - A very large brewery. Likely not for visitors. Ex. Miller-Coors. (deprecated)
 - *planning* - A brewery in planning or not yet opened to the public.
 - *bar* - A bar. No brewery equipment on premise. (deprecated)
 - *contract* - A brewery that uses another brewery's equipment.
 - *proprietor* - Similar to contract brewing but refers more to a brewery incubator.
 - *closed* - A location which has been closed.

#### 4.2.2 Result list

##### 4.2.2.1 Query parameters

On the top of the result list you should always see the parameters of the query you see the result of.

##### 4.2.2.2 Brewery Cards

The breweries are represented as cards. They provide basic information and three actions:
 - Link: will open the website of the brewery
 - Map: will open Google Maps and navigate to the location of the brewery
 - Favourite toggle: adds / removes the brewery to / from the Favourites list

##### 4.2.2.3 Fetch More

Every execution of all queries return maximum 20 breweries. To fetch further breweries for the last query
you can use the *FETCH MORE* button at the end of the result list. The button will not be displayed
after you fetched all the breweries.

### 4.3 Favourites

This list displays the breweries you added to your favourites list. Regarding form and functionality it is
identical to **4.2.2.2 Brewery Cards**.

Of course, when you toggle a card in favourites list, it will disappear.

## 5 To be done later

 - UX design is poor
 - Error handling / displaying errors (e.g.: invalid filter patterns)
 - Tests. The environment is ready, but virtually no unit tests were added.
 - There were left some TODO-s in the code.

## 6 Takeaways

### 6.1 UX

Well, UX has never been my strong suite. That said, I found MUI easy to work with.
They changed a lot of things in MUI 5 though, so I had to pick it up again, which took more time thanI estimated.

What I haven't worked out was themes in MUI. It was hard to understand how it works exactly, so I solved most styling
with scss modules.

### 6.2 React + redux + rxjs

Tbh, I find Angular + NGXS more convenient, but I must admit React is more customisable and seems to be less resource heavy.